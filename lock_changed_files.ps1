# Check if running in a Git repository
if (-not (Test-Path .git)) {
    Write-Error "This script must be run from the root of a Git repository."
    exit 1
}

# Find changed binary files
$changed_files = git diff --cached --name-only --diff-filter=ACM | Select-String -Pattern '\.(uasset|umap|fbx|png)$'

# Check if there are any changed binary files
if ($changed_files.Count -eq 0) {
    Write-Output "No binary files to lock."
    exit 0
}

# Lock each file
foreach ($file in $changed_files) {
    Write-Output "Locking $($file.Line)"
    git lfs lock "$($file.Line)"
}

exit 0